#include "tree.h"

#define COUNT 10

// Helper function to allocates a new node 
treeNode* newTreeNode(int id) { 
    treeNode* node = malloc(sizeof(treeNode)); 
    node->songID = id; 
    node->lc = node->rc = NULL; 
    return node;
}

int count_tree_nodes(treeNode *tree){ //root of subtree
    int count = 1; //node itself should be ountounted
    if (tree == NULL) return 0;
    else{
        count += count_tree_nodes(tree->lc);
        count += count_tree_nodes(tree->rc);
    }
    return count;
}

int key_sum_check(treeNode *tree){ //root of subtree
    int sum = 0;
    if (tree != NULL){
        sum = tree->songID;
        sum += key_sum_check(tree->lc);
        sum += key_sum_check(tree->rc);
    }
    return sum;
}

void tree_init(){
    sentinel = (treeNode*)malloc(sizeof(treeNode));
    root = (treeNode*)malloc(sizeof(treeNode));
    pthread_mutex_init(&sentinel->lock, NULL);
    pthread_mutex_init(&root->lock, NULL);
    sentinel->songID = -1;
    root->songID = -1;

    sentinel->rc = root;
}

int BSTInsert(int songID){
    treeNode *curr, *pred, *newnode;
    //printf("insert %d: ",songID);

    pthread_mutex_lock(&root->lock);

    if(root->songID == -1){
        //printf("\nCREATE NEW NODE\n");     
        //printf("root songID is %d\n",songID);
        root->songID = songID;
        root->lc = NULL;
        root->rc = NULL;
        pthread_mutex_unlock(&root->lock);
        return 1;
    }

    if(root->songID == songID){
        printf("id %d already in tree\n",songID);
        pthread_mutex_unlock(&root->lock);
        return 0;
    }
    pred = root;
    if(songID < pred->songID) curr = pred->lc;
    else curr = pred->rc;

    if(curr != NULL) pthread_mutex_lock(&curr->lock);

    while(curr != NULL){
        pthread_mutex_unlock(&pred->lock);
        if(curr->songID == songID){
            printf("id %d already in tree\n",songID);
            pthread_mutex_unlock(&curr->lock);
            return 0;
        }
        pred = curr;
        if(songID < curr->songID) curr = curr->lc;
        else curr = curr->rc;

        if(curr != NULL) pthread_mutex_lock(&curr->lock);
    }

    //printf("\nCREATE NEW NODE\n");

    newnode = (treeNode*)malloc(sizeof(treeNode));
    pthread_mutex_init(&newnode->lock, NULL);
    newnode->songID = songID;
    newnode->lc = NULL;
    newnode->rc = NULL;

    if(songID < pred->songID) pred->lc = newnode;
    else pred->rc = newnode;

    //printf("songID is %d\n",songID);

    pthread_mutex_unlock(&pred->lock);

    //pthread_mutex_destroy(&pred->lock);
    //if(curr != NULL) pthread_mutex_destroy(&curr->lock);
    return 1;
}

treeNode *BSTSearch(int songID){
    treeNode *curr, *pred, *result;

    if(root->songID == -1) return NULL;

    pthread_mutex_lock(&root->lock);
    if(songID == root->songID){
        result = root;
        //pthread_mutex_unlock(&root->lock);
        return result;
    }
    pred = root;
    if(songID < pred->songID) curr = pred->lc;
    else curr = pred->rc;

    pthread_mutex_lock(&curr->lock);

    while(curr && songID != curr->songID){
        pthread_mutex_unlock(&pred->lock);
        pred = curr;
        if(songID < curr->songID) curr = curr->lc;
        else curr = curr->rc;
        pthread_mutex_lock(&curr->lock);
    }

    if(songID == curr->songID) result = curr;
    else result = NULL;

    pthread_mutex_unlock(&pred->lock);
    //pthread_mutex_unlock(&curr->lock);

    return result; /* result is locked */
}

treeNode *findParent(treeNode *child){ /* child is locked */

    treeNode *curr, *pred = NULL;

    if(child == root) return NULL;

    curr = root;
    pthread_mutex_lock(&curr->lock); //edoooooo kollaei
    while(curr != child && curr != NULL){

        pred = curr;
        if(child->songID < curr->songID)
            curr = curr->lc;
        else
            curr = curr->rc;

        if(curr != child){
            pthread_mutex_lock(&curr->lock);
        }
        pthread_mutex_unlock(&pred->lock);
    }

    if(curr != NULL) return pred;

    return NULL;
}

treeNode *minValueNode(treeNode* node){ /* node is locked */

    treeNode *pred, *curr = node; /* inOrderSuccessor: node->rc */
  
    /* loop down to find the leftmost leaf */
    while (curr && curr->lc != NULL){
        pred = curr;
        curr = curr->lc;
        pthread_mutex_lock(&curr->lock);
        pthread_mutex_unlock(&pred->lock);
    }
    //pthread_mutex_unlock(&curr->lock);
    return curr; 
}

treeNode *inOrderSuccessor(treeNode *node) { /* node is locked */
    
    /* If right subtree of node is not NULL, then succ lies in 
    right subtree -> it is the node with minimum key value */
    if(node->rc != NULL){
        pthread_mutex_lock(&node->rc->lock);
        return minValueNode(node->rc); /* return locked */
    }
    /* NEVER reaching this point ... */
    treeNode *succ = NULL, *tmp;
  
    tmp = root;
    /* If right sbtree of node is NULL, start from root and search 
    for successor down the tree */
    while (tmp != NULL) { 
        if (node->songID < tmp->songID){ 
            succ = tmp; 
            tmp = tmp->lc;
        } 
        else if (node->songID > tmp->songID){
            tmp = tmp->rc;
        }else break;
    }  
    return succ;
}

treeNode *BSTDelete(int songID){

    treeNode *z, *y, *x, *zparent, *yparent;

    /*----- search node with songID and his parent -----*/

    if(root->songID == -1) return NULL;

    pthread_mutex_lock(&sentinel->lock);
    pthread_mutex_lock(&root->lock);

    if(root->songID == songID){
        z = root;
        zparent = sentinel;
        //pthread_mutex_lock(&zparent->lock);
    }else{
        pthread_mutex_unlock(&sentinel->lock);
        zparent = root;
        if(songID < zparent->songID) z = zparent->lc;
        else z = zparent->rc;

        pthread_mutex_lock(&z->lock);

        while(z!=NULL && z->songID != songID){
            pthread_mutex_unlock(&zparent->lock);
            zparent = z;
            if(songID < z->songID) z = z->lc;
            else z = z->rc;

            if(z != NULL) {pthread_mutex_lock(&z->lock);}
            else {
                pthread_mutex_unlock(&zparent->lock);   
                return NULL;
            }
        }
    }

    /* z and zparent are locked */

    //printf("DELETING SONG %d\n",z->songID);

    if(z->lc == NULL || z->rc == NULL){
        y = z;
        yparent = zparent;
    }else{
        //y = inOrderSuccessor(z);

        y = z->rc;
        pthread_mutex_lock(&y->lock);

        yparent = z;

        /* loop down to find the leftmost leaf */
        while (y && y->lc != NULL){
            yparent = y;
            y = y->lc;
            pthread_mutex_lock(&y->lock);
            /* an exw vrei to leftmost leaf krataw ton patera */
            if(y->lc != NULL){
                pthread_mutex_unlock(&yparent->lock);
            }
        }
        /* y and yparent locked */
    }

    if(y->lc != NULL) x = y->lc;
    else x = y->rc;

    if(x != NULL){ pthread_mutex_lock(&x->lock);}

    /* root deletion */
    if(yparent == sentinel){
        pthread_mutex_unlock(&z->lock);
        if(y != z) {pthread_mutex_unlock(&y->lock);}
        if(x != NULL) {pthread_mutex_unlock(&x->lock);}
        pthread_mutex_unlock(&yparent->lock);
        if(zparent != NULL) {pthread_mutex_unlock(&zparent->lock);}
        return x;
    }else if(y == yparent->lc){
        /* y is left child */
        yparent->lc = x;
    }else 
        yparent->rc = x;

    if(y != z){
        z->songID = y->songID;
        pthread_mutex_unlock(&y->lock);
    }
    pthread_mutex_unlock(&z->lock);

    if(x != NULL) {pthread_mutex_unlock(&x->lock);}

    if(zparent != NULL) {pthread_mutex_unlock(&zparent->lock);}

    if(yparent != z && yparent != zparent && yparent != NULL){
        pthread_mutex_unlock(&yparent->lock);
    }

    return root;
}

/* print binary search tree */
void printBST(treeNode *root, int space) 
{ 
    if(root == NULL) return; 
  
    // increase distance between levels 
    space += COUNT; 
  
    // right child first 
    printBST(root->rc, space); 
    
    // print current node after space 
    printf("\n"); 
    int i;
    for(i = COUNT; i < space; i++) 
        printf(" "); 
    printf("%d\n", root->songID); 
  
    // process left child 
    printBST(root->lc, space); 
}

int square_root(int num){
    return num*num;
}
void tree_checks(){

    int total_size = 0, total_keysum = 0;
    int expected_size, expected_keysum;

    /* 1. Tree size check */
    total_size = count_tree_nodes(root);
    expected_size = square_root(N);
    if(total_size != expected_size){
        printf("\033[1;31m");
        printf("tree's total size check failed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
        exit(0);
    }else{
        printf("\033[1;32m");
        printf("tree's total size check passed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
    }

    /* 2. Total keysum check */
    total_keysum = key_sum_check(root);
    expected_keysum = square_root(N)*(N-1)*(N+1)/2;
    if(total_keysum != expected_keysum){
        printf("\033[1;31m");
        printf("total keysum check failed (expected: %d, found: %d)\n",expected_keysum,total_keysum);
        printf("\033[0m");
        exit(0);
    }else{
        printf("\033[1;32m");
        printf("total keysum check passed (expected: %d, found: %d)\n",expected_keysum,total_keysum);
        printf("\033[0m");
    }
    return; 
}