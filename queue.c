#include "queue.h"

queueNode *newQNode(int id) { 
    queueNode *node = malloc(sizeof(queueNode)); 
    node->songID = id; 
    node->next = NULL; 
    return node; 
}

void queue_init(){
    int i;
    Queue = malloc(sizeof(UnboundedTotalQueue)*M); /* initial queues */

    for(i=0; i<M; i++){
        Queue[i].Head = newQNode(-1); /* sentinel */
        Queue[i].Tail = Queue[i].Head;
    }
    //init headLock/tailLock
}

/* puts item x at the tail */
void enqueue(int x, int qindex) {

    queueNode *n = newQNode(x);

    pthread_mutex_lock(&Queue[qindex].tailLock);
    Queue[qindex].Tail->next = n;
    Queue[qindex].Tail = n; /* takes its place */
    pthread_mutex_unlock(&Queue[qindex].tailLock);
}

/* removes an item from head */
int dequeue(int qindex) {

    int result;
    pthread_mutex_lock(&Queue[qindex].headLock);
    if(Queue[qindex].Head->next == NULL)
        result = -1; /* empty queue */
    else {
        result = Queue[qindex].Head->next->songID;
        Queue[qindex].Head = Queue[qindex].Head->next;
    }
    pthread_mutex_unlock(&Queue[qindex].headLock);
    return result;
}

void printQueue(queueNode *head){

    queueNode *tmp = head->next;

    while(tmp != NULL){
        printf("%d  ", tmp->songID);
        tmp = tmp->next;
    }
    printf("\n");
}

int count_nodes(queueNode *head){

    int count = 0;
    queueNode *tmp = head;

    while(tmp != NULL){
        tmp = tmp->next;
        count++;
    }
    return count-1; /*minus the sentinel*/
}

int count_sum(queueNode *head){

    int sum = 0;
    queueNode *tmp = head->next;

    while(tmp != NULL){
        sum += tmp->songID;
        tmp = tmp->next;
    }
    return sum;
}

void queue_checks(){

    int total_size = 0, total_keysum = 0;
    int expected_size = N*N, expected_keysum;

    int i, counted[M], sums[M];

    /* 1. Queue size check */

    for(i=0; i<M; i++){
        counted[i] = count_nodes(Queue[i].Head);
        if(counted[i] != 2*N){
            printf("\033[1;31m");
            printf("total size check of Queue %d failed (expected: %d, found: %d)\n",i,2*N,counted[i]);
            printf("\033[0m");
            exit(0);
        }
        sums[i] = count_sum(Queue[i].Head);
    }

    for(i=0; i<M; i++){
        total_size += counted[i];
        total_keysum += sums[i];
    }

    if(total_size != expected_size){
        printf("\033[1;31m");
        printf("queues' total size check failed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
        exit(0);
    }else{
        printf("\033[1;32m");
        printf("queues' total size check passed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
    }

    /* 2. Total keysum check */

    expected_keysum = N*N*(N-1)*(N+1)/2;

    if(total_keysum != expected_keysum){
        printf("\033[1;31m");
        printf("total keysum check failed (expected: %d, found: %d)\n",expected_keysum,total_keysum);
        printf("\033[0m");
        exit(0);
    }else{
        printf("\033[1;32m");
        printf("total keysum check passed (expected: %d, found: %d)\n",expected_keysum,total_keysum);
        printf("\033[0m");
    }

    return; 
}
