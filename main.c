#include "tree.h"
#include "queue.h"
#include "list.h"

pthread_barrier_t barrier1_phase1, barrier2_phase1;
pthread_barrier_t barrier1_phase2, barrier2_phase2;
pthread_barrier_t barrier_phase3;

// Wrapper over printBST() 
void print2Dtree(treeNode *root) 
{ 
   // initial space count 0 
   printBST(root, 0); 
}

void final_checks(){

    int total_size = 0;
    int expected_size;
    int i, counted[M];

    /* 1. Tree size check */

    total_size = count_tree_nodes(root);

    expected_size = N*N/2;

    if(total_size != expected_size){
        printf("\033[1;31m");
        printf("tree's size check failed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
        exit(0);
    }else{
        printf("\033[1;32m");
        printf("tree's size check passed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
    }

    /* 2. Queue size check */

    for(i=0; i<M; i++){
        counted[i] = count_nodes(Queue[i].Head);
        if(counted[i] != N){
            printf("\033[1;31m");
            printf("total size check of Queue %d failed (expected: %d, found: %d)\n",i,N,counted[i]);
            printf("\033[0m");
            exit(0);
        }
    }
    total_size = 0;
    for(i=0; i<M; i++){
        total_size += counted[i];
    }
    //expected_size = N*N/2;
    if(total_size != expected_size){
        printf("\033[1;31m");
        printf("queues' total size check failed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
        exit(0);
    }else{
        printf("\033[1;32m");
        printf("queues' total size check passed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
    }

    /* 3. List size check */

    total_size = count_list_nodes(LinkedList.head);

    if(total_size != expected_size){
        printf("\033[1;31m");
        printf("list’s size check failed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
        exit(0);
    }else{
        printf("\033[1;32m");
        printf("list’s size check passed (expected: %d, found: %d)\n",expected_size,total_size);
        printf("\033[0m");
    }
}

void *thread_code(void *thread_id){

    /* --- thread producers --- */

    int i, j = *((int*)thread_id);
    //printf("I am thread %d\n",j);

    for(i=0; i<N; i++){
        BSTInsert(i*N + j);
    }
    pthread_barrier_wait(&barrier1_phase1);
    //printf("I am thread %d\n",j);
    
    if(j == 0) tree_checks();

    pthread_barrier_wait(&barrier2_phase1);

    /* --- thread users --- */

    //printf("I am thread %d\n",j);
    
    int qindex = (j+1)%M;
    treeNode *node;
    for(i=0; i<N; i++){
        //printf("qindex: %d\n",qindex);
        node = BSTSearch(j*N + i); /* node is locked */

        pthread_mutex_unlock(&node->lock);

        enqueue(node->songID, qindex);
        if(qindex == M-1) qindex = -1;
        qindex++;
    }

    pthread_barrier_wait(&barrier1_phase2);

    if(j == 0) queue_checks();

    pthread_barrier_wait(&barrier2_phase2);
    //printf("I am thread %d\n",j);

    /* --- system administrators --- */
    int song; // the song to be deleted
    qindex = (j+1)%M;

    for(i=0; i<(N/2); i++){
        song = dequeue(qindex);
        if(qindex == M-1) qindex = -1;
        qindex++;
        root = BSTDelete(song);
        list_insert(song);
    }

    pthread_barrier_wait(&barrier_phase3);
    //printf("I am thread %d\n",j);

    if(j == 0) final_checks();
}

int main(int argc, char** argv){

    int i,j,b,rc = 0;
    int *threadIDs;

    if(argc != 2){
		printf("Wrong input.\n");
		exit(0);
	}
    N = atoi(argv[1]);
    printf("threads: %d\n",N);

    if(N%2 != 0 || N <= 0){ // in order to create the queues
        printf("\nNumber of threads must be a positive even number!\n");
        exit(0);
    } // trees work fine with odd number of threads

    tree_init();

    pthread_t threads[N];

    threadIDs = (int*)malloc(sizeof(int)*N);
    /* initialize the thread IDs */
    for(i=0; i<N; i++){
        threadIDs[i] = i;
    }

    b = pthread_barrier_init(&barrier1_phase1, NULL, N);
    if (b){
        printf("ERROR: return code from pthread_barrier_init() is %d\n", b);
        exit(-1);
    }
    b = pthread_barrier_init(&barrier2_phase1, NULL, N);
    if (b){
        printf("ERROR: return code from pthread_barrier_init() is %d\n", b);
        exit(-1);
    }
    b = pthread_barrier_init(&barrier1_phase2, NULL, N);
    if (b){
        printf("ERROR: return code from pthread_barrier_init() is %d\n", b);
        exit(-1);
    }
    b = pthread_barrier_init(&barrier2_phase2, NULL, N);
    if (b){
        printf("ERROR: return code from pthread_barrier_init() is %d\n", b);
        exit(-1);
    }
    b = pthread_barrier_init(&barrier_phase3, NULL, N);
    if (b){
        printf("ERROR: return code from pthread_barrier_init() is %d\n", b);
        exit(-1);
    }

    M = N/2;
    printf("queues: %d\n",M);
    queue_init();

    list_init();
    
    for(j=0; j<N; j++){
        rc = pthread_create(&threads[j], NULL, thread_code, &threadIDs[j]);
        if (rc) {
            printf("ERROR: return code from pthread_create() is %d\n", rc);
            exit(-1);
	    }
    }

    for(i=0; i<N; i++){
        //printf("join %d\n",i);
        rc = pthread_join(threads[i], NULL);
        if (rc) {
            printf("ERROR: return code from pthread_join() is %d\n", rc);
            exit(-1);
	    }
    }

    //print2Dtree(root);

    printf("\n");

    for(i=0; i<M; i++){
        printf("Queue %d: ",i);
        printQueue(Queue[i].Head);
    }

    print2Dtree(root);

    print_list();

    pthread_barrier_destroy(&barrier1_phase1);
    pthread_barrier_destroy(&barrier2_phase1);
    pthread_barrier_destroy(&barrier1_phase2);
    pthread_barrier_destroy(&barrier2_phase2);
    pthread_barrier_destroy(&barrier_phase3);
    free(threadIDs);

    return 0;
}