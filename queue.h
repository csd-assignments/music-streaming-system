#ifndef QUEUE_H_
#define QUEUE_H_

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "common.h"

typedef struct queueNode {
    int songID;
    struct queueNode *next;
}queueNode;

typedef struct queue {
    struct queueNode *Head;
    struct queueNode *Tail;
    pthread_mutex_t headLock;
    pthread_mutex_t tailLock;
}UnboundedTotalQueue;

UnboundedTotalQueue *Queue; /* array of M queues */

int M; /* number of queues */

void queue_init();
void printQueue(queueNode *head);

void enqueue(int x, int qindex);
int dequeue(int qindex);

int count_nodes(queueNode *head);
int count_sum(queueNode *head);

void queue_checks();

#endif /* QUEUE_H_ */