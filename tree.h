#ifndef TREE_H_
#define TREE_H_

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "common.h"

typedef struct treeNode {
    int songID;
    struct treeNode *lc;
    struct treeNode *rc;
    pthread_mutex_t lock;
}treeNode;

treeNode *root, *sentinel;

void tree_init();

treeNode *BSTSearch(int songID);
int BSTInsert(int songID);
treeNode *BSTDelete(int songID);

treeNode *minValueNode(treeNode* node);
treeNode *findParent(treeNode *child);

int count_tree_nodes(treeNode *tree);
int key_sum_check(treeNode *tree);

void tree_checks();

void printBST(treeNode *root, int space);

#endif /* TREE_H_ */