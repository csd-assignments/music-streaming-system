#ifndef LIST_H_
#define LIST_H_

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

typedef struct listNode {
    int songID;
    struct listNode *next;
    pthread_mutex_t lock;
}listNode;

struct list {
    listNode *head;
    listNode *tail;
};

struct list LinkedList;

int validate(listNode *pred, listNode *curr);
int list_insert(int songID);

void list_init();
void print_list();

int count_list_nodes(listNode *head);

#endif /* LIST_H_ */