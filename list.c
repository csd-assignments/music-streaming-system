#include "list.h"

listNode *newListNode(int id) { 
    listNode *node = malloc(sizeof(listNode)); 
    node->songID = id; 
    node->next = NULL;
    pthread_mutex_init(&node->lock,NULL);
    return node;
}

void list_init(){
    LinkedList.head = newListNode(-1); //sentinel
    LinkedList.tail = LinkedList.head; 
}

int validate(listNode *pred, listNode *curr) {

    listNode *tmp = LinkedList.head;

    while (tmp->songID <= pred->songID) {
        if (tmp == pred) {
            if (pred->next == curr) return 1;
            else return 0;
        }
        tmp = tmp->next;
    }
    return 0;
}

int list_insert(int songID) {

    listNode *pred, *curr;
    int result;
    int return_flag = 0;

    //printf("list insert %d\n",songID);

    while (1) {
        pred = LinkedList.head; 
        curr = pred->next;

        while (curr != NULL && curr->songID < songID) {
            pred = curr;
            curr = curr->next;
        }
        pthread_mutex_lock(&pred->lock);
        if(curr == NULL){
            listNode *node = newListNode(songID);
            node->next = curr;
            pred->next = node;
            pthread_mutex_unlock(&pred->lock);
            return 1;
        }
        pthread_mutex_lock(&curr->lock);

        if (validate(pred, curr) == 1) { /* pred->next == curr */

            if (songID == curr->songID) {
                result = 0; return_flag = 1;
            } else {
                listNode *node = newListNode(songID);
                node->next = curr;
                pred->next = node;
                result = 1; return_flag = 1;
            }
        }
        pthread_mutex_unlock(&pred->lock);
        pthread_mutex_unlock(&curr->lock);

        if(return_flag) return result;
    }
    printf("failure\n");
    return -1;
}

int count_list_nodes(listNode *head){

    int count = 0;
    listNode *tmp = head;

    while(tmp != NULL){
        tmp = tmp->next;
        count++;
    }
    return count-1; /*minus the sentinel*/
}

void print_list(){

    listNode *tmp = LinkedList.head->next;

    printf("\nLinked List: ");
    while(tmp != NULL){
        printf("%d ",tmp->songID);
        tmp = tmp->next;
    }
    printf("\n");
}